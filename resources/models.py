from django.db import models

# Create your models here.
class Tag(models.Model):
    name = models.CharField(max_length=20)
    parents = models.ManyToManyField('self')

class Resource(models.Model):
    name = models.CharField(max_length=50)
    type = models.CharField(
        max_length=3,
        choices=[
            ('VID', 'Video'),
            ('FOR', 'Forum Post'),
            ('PAP', 'Paper'),
            ('CAD', 'CAD'),
            ('WEB', 'Website'),
            ('COD', 'Code'),
            ('OTH', 'Other')
        ],
        default='OTH'
    )
    
    tags = models.ManyToManyField(
        Tag,
        through='ResourceTag',
        through_fields=('resource', 'tag')
    )

    url = models.URLField()
    description = models.TextField()
    submitted_by = models.CharField(max_length=50)



class TagAlternative(models.Model):
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)

class ResourceTag(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)

