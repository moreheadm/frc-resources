from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('resource/<int:resource_id>', views.resource, name='resource'),
    path('search/<search_string>', views.search, name='search')
]
