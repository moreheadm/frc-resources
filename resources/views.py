from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    return HttpResponse('Hello world')

def resource(request, resource_id):
    return HttpResponse('Requested resource {}'.format(resource_id))

def search(request, search_string):
    return HttpResponse('Search for {}'.format(search_string))
